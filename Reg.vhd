----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:02:54 04/09/2020 
-- Design Name: 
-- Module Name:    Reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Reg is
    generic (n : natural :=4);
    Port ( load, increment, clear, clock : in STD_LOGIC;
			  i : in  STD_LOGIC_VECTOR (n-1 downto 0);
           q : out  STD_LOGIC_VECTOR (n-1 downto 0));
end Reg;

architecture Behavioral of Reg is
SIGNAL temp : STD_LOGIC_VECTOR (n-1 downto 0);
begin
process
begin
if (clock' event and clock = '1') then
if (load = '1') then
temp <= i;
elsif (increment = '1') then
temp <= temp + 1;
elsif (clear = '1') then
temp <= ( others => '0');
end if; 
end if; 
q <= temp;
end process;
end Behavioral;

