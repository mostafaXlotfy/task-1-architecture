--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
library work;
use IEEE.STD_LOGIC_1164.all;

package mypack is

component Reg is
    generic (n : natural :=4);
    Port ( load, increment, clear, clock : in STD_LOGIC;
			  i : in  STD_LOGIC_VECTOR (n-1 downto 0);
           q : out  STD_LOGIC_VECTOR (n-1 downto 0));
end component;

component Decoder is
    Port ( e : in BIT ; 
			  i : in  STD_LOGIC_VECTOR (4 downto 0);
           q : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component mux is
    Port ( i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19,i20,i21
				,i22,i23,i24,i25,i26,i27,i28,i29,i30,i31 : in  STD_LOGIC_VECTOR (31 downto 0);
           q : out  STD_LOGIC_VECTOR (31 downto 0);
           selector : in  STD_LOGIC_VECTOR (4 downto 0));
end component;

end myPack;
