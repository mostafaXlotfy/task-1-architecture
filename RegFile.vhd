----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:00:15 04/09/2020 
-- Design Name: 
-- Module Name:    RegFile - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.mypack.ALL;
entity RegFile is
    Port ( writeEnable : in BIT;
			  rs,rt,rd  : in STD_LOGIC_VECTOR (4 downto 0);
			  writeData : in  STD_LOGIC_VECTOR (31 downto 0);
           data1,data2 : out  STD_LOGIC_VECTOR (31 downto 0));
end RegFile;

architecture Behavioral of RegFile is
signal o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19,o20,o21,o22,o23,o24,o25,o26,o27,o28,o29,o30,o31:STD_LOGIC_VECTOR (31 downto 0);
signal decoderOutput, muxOutput1, muxOutput2 :STD_LOGIC_VECTOR (31 downto 0);
signal clock:STD_LOGIC;	 	 
begin

decoder : Decoder port map ( writeEnable, rd, decoderOutput);
ro   : Reg generic map (32) port map ( '0' , '0', '1'   , clock, writeData, o0)  ;
r1   : Reg generic map (32) port map ( decoderOutput(1) , '0', '0' , clock, writeData, o1)  ;
r2   : Reg generic map (32) port map ( decoderOutput(2) , '0', '0' , clock, writeData, o2)  ;
r3   : Reg generic map (32) port map ( decoderOutput(3) , '0', '0' , clock, writeData, o3)  ;
r4   : Reg generic map (32) port map ( decoderOutput(4) , '0', '0' , clock, writeData, o4)  ;
r5   : Reg generic map (32) port map ( decoderOutput(5) , '0', '0' , clock, writeData, o5)  ;
r6   : Reg generic map (32) port map ( decoderOutput(6) , '0', '0' , clock, writeData, o6)  ;
r7   : Reg generic map (32) port map ( decoderOutput(7) , '0', '0' , clock, writeData, o7)  ;
r8   : Reg generic map (32) port map ( decoderOutput(8) , '0', '0' , clock, writeData, o8)  ;
r9   : Reg generic map (32) port map ( decoderOutput(9) , '0', '0' , clock, writeData, o9)  ;
r10  : Reg generic map (32) port map ( decoderOutput(10), '0', '0' , clock, writeData, o10) ;
r11  : Reg generic map (32) port map ( decoderOutput(11), '0', '0' , clock, writeData, o11) ;
r12  : Reg generic map (32) port map ( decoderOutput(12), '0', '0' , clock, writeData, o12) ;
r13  : Reg generic map (32) port map ( decoderOutput(13), '0', '0' , clock, writeData, o13) ;
r14  : Reg generic map (32) port map ( decoderOutput(14), '0', '0' , clock, writeData, o14) ;
r15  : Reg generic map (32) port map ( decoderOutput(15), '0', '0' , clock, writeData, o15) ;
r16  : Reg generic map (32) port map ( decoderOutput(16), '0', '0' , clock, writeData, o16) ;
r17  : Reg generic map (32) port map ( decoderOutput(17), '0', '0' , clock, writeData, o17) ;
r18  : Reg generic map (32) port map ( decoderOutput(18), '0', '0' , clock, writeData, o18) ;
r19  : Reg generic map (32) port map ( decoderOutput(19), '0', '0' , clock, writeData, o19) ;
ro20 : Reg generic map (32) port map ( decoderOutput(20), '0', '0' , clock, writeData, o20) ;
ro21 : Reg generic map (32) port map ( decoderOutput(21), '0', '0' , clock, writeData, o21) ;
ro22 : Reg generic map (32) port map ( decoderOutput(22), '0', '0' , clock, writeData, o22) ;
ro23 : Reg generic map (32) port map ( decoderOutput(23), '0', '0' , clock, writeData, o23) ;
ro24 : Reg generic map (32) port map ( decoderOutput(24), '0', '0' , clock, writeData, o24) ;
ro25 : Reg generic map (32) port map ( decoderOutput(25), '0', '0' , clock, writeData, o25) ;
ro26 : Reg generic map (32) port map ( decoderOutput(26), '0', '0' , clock, writeData, o26) ;
ro27 : Reg generic map (32) port map ( decoderOutput(27), '0', '0' , clock, writeData, o27) ;
ro28 : Reg generic map (32) port map ( decoderOutput(28), '0', '0' , clock, writeData, o28) ;
ro29 : Reg generic map (32) port map ( decoderOutput(29), '0', '0' , clock, writeData, o29) ;
ro30 : Reg generic map (32) port map ( decoderOutput(30), '0', '0' , clock, writeData, o30) ;
ro31 : Reg generic map (32) port map ( decoderOutput(31), '0', '0' , clock, writeData, o31) ;

mux1 : mux port map ( o0,o1,02,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19,o20,o21
		 ,o22,o23,o24,o25,o26,o27,o28,o29,o30,o31, muxOutput1, rs);
mux2 : mux port map ( o0,o1,02,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19,o20,o21
		 ,o22,o23,o24,o25,o26,o27,o28,o29,o30,o31, muxOutput2, rt);
data1 >= muxOutput1;
data2 >= muxOutput2;
end Behavioral;

